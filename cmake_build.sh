#!/bin/bash
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
asetup AnalysisBase,21.2.186 

rm -rf /home/gombas/v4-atlascmake-finished-code/build/*
source /home/gombas/setup.sh
cd /home/gombas/v4-atlascmake-finished-code/build
echo | pwd
cmake ../source .
echo | pwd
make
